import React from "react";

import Menu from "./Menu";
import MenuItem from "./MenuItem";

export default class Header extends React.Component {

  constructor(props) {
    super(props);
  }

  leftShow(){
    this.refs.left.show();
  }
  leftHide(){
    this.refs.left.hide();
  }

  render() {
    return (
      <div className="">
        <div className="container">
          <div className="header">
            <div className="col-12">
              <img src="images/logo.png" className="logo"/>
              <a onClick={this.leftShow.bind(this)} className="pull-right"><img src="images/burger-menu.png" className="menu-icon"/></a>
              <Menu ref="left" alignment="left">
              <a onClick={this.leftHide.bind(this)} className="close-icon"><img src="images/close.png" className="menu-icon"/></a>
                <img src="images/logo.png" className="logo"/>
                <MenuItem hash="About">About</MenuItem>
                <MenuItem hash="Services">Services</MenuItem>
                <MenuItem hash="Work">Work</MenuItem>
                <MenuItem hash="Clients">Clients</MenuItem>
                <MenuItem hash="Contact">Contact</MenuItem>
                <div className="submenu">
                  <ul>
                    <MenuItem hash="news"><li className="none">Latest News</li></MenuItem>
                    <MenuItem hash="career"><li>Career</li></MenuItem>
                  </ul>
                </div>
                <div className="social">
                  <h3>Follow us on</h3>
                  <ul>
                    <a href="#"><li className="fb"></li></a>
                    <a href="#"><li className="insta"></li></a>
                    <a href="#"><li className="tw"></li></a>
                  </ul>
                </div>
              </Menu>
            </div>
          </div>
        </div>
        
      </div>
    );
  }
}
