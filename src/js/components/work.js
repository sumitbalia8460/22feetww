import React from "react";


export default class Work extends React.Component {

render() {
	var slideone = "../images/car.jpg";
	var slidetwo = "../images/men.jpg";
	var sliderone = {
	  background: 'url(' + slideone + ') center no-repeat #333',
	};
	var slidertwo = {
	  background: 'url(' + slidetwo + ') center no-repeat #333',
	};
    return (
      <div className="container mtb15 work">
       <div>
       	<h2 className="text-center">Latest Work</h2>
       	<div>
       		<div className="swiper-container work-slider">
		        <div className="swiper-wrapper">
		            <div className="swiper-slide" style={sliderone}>
		            	<a href="#" className="overlay">
		            		<div className="caption">
			            		<p className="small-heading">Website</p>
			            		<h3 className="heading">Beetle</h3>
			            	</div>
		            	</a>
		            </div>
		            <div className="swiper-slide" style={slidertwo}>
		            	<a href="" className="overlay">
		            		<div className="caption">
			            		<p className="small-heading">Website</p>
			            		<h3 className="heading">Raymond</h3>
			            	</div>
		            	</a>
		            </div>
		            <div className="swiper-slide" style={sliderone}>
		            	<a href="" className="overlay">
		            		<div className="caption">
			            		<p className="small-heading">Website</p>
			            		<h3 className="heading">Beetle</h3>
			            	</div>
		            	</a>
		            </div>
		            <div className="swiper-slide" style={sliderone}>
		            	<a href="" className="overlay">
		            		<div className="caption">
			            		<p className="small-heading">Website</p>
			            		<h3 className="heading">Beetle</h3>
			            	</div>
		            	</a>
		            </div>
		        </div>
		    </div>
       	</div>
       </div>
      </div>
      
    );
  }

}