import React from "react";

import Header from "./Header";
import Carousel from "./Carousel";
import Work from "./Work";
import News from "./News";
import Footer from "./Footer";


export default class Layout extends React.Component {
  
  render() {
    return (
      <div>
        <Header/>
        <Carousel/>
        <Work/>
        <News/>
        <Footer />
      </div>
    );
  }
}
