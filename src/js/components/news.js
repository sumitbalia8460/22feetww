import React from "react";


export default class News extends React.Component {

render() {
    return (
      <div className="container mtb15 work">
       <div>
       	<h2 className="text-center">Latest News</h2>
       	<div>
       		<div className="swiper-container work-slider">
		        <div className="swiper-wrapper">
		            <div className="swiper-slide slide-one">
		            	<a href="#" className="">
		            		<div className="news-caption">
		            			<h3>Vineet Gupta and Aditya Kanthy are DDB Mudra Group's new Flag bearers</h3>
			            		<p className="small-heading caption-box">Et in laboris irure aliquip in dolore reprehenderit exercitation commodo laborum voluptate.Et in laboris irure aliquip in dolore reprehenderit exercitation commodo laborum voluptate.</p>
			            		
			            	</div>
		            	</a>
		            </div>
		            <div className="swiper-slide slide-two">
		            	<a href="" className="">
		            		<div className="news-caption">
		            			<h3>DDB Mudra</h3>
			            		<p className="small-heading caption-box">Et in laboris irure aliquip in dolore reprehenderit exercitation commodo laborum voluptate Et in laboris irure aliquip in dolore reprehenderit exercitation commodo laborum voluptate..</p>
			            		
			            	</div>
		            	</a>
		            </div>
		        </div>
		    </div>
       	</div>
       </div>
      </div>
      
    );
  }

}