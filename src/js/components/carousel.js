import React from "react";

// load in JSON data from file
var data;

var dataReq = new XMLHttpRequest();
dataReq.onload = reqListener;
dataReq.open("get", "../../data.json", true);
dataReq.send();

function reqListener(e) {
	var responseText;
    data = JSON.parse(this.responseText);
    console.log(data);
}

export default class Carousel extends React.Component {

render() {
	var slideone = "../images/slider-bg.jpg";
	var sliderone = {
	  background: 'url(' + slideone + ') center no-repeat #333',
	};
	
    return (
      <div className="home-slider">
       <div className="">
       	<div className="swiper-container home-slide">
	        <div className="swiper-wrapper">
	            <div className="swiper-slide" style={sliderone}>
	            	<div className="overlay">
	            		<div className="caption">
		            		<p className="small-heading">Asian Paints</p>
		            		<p className="heading">An Untold Story</p>
		            		<a href="" className="btn">View Case Study</a>
		            	</div>
	            	</div>
	            </div>
	            <div className="swiper-slide" style={sliderone}>
	            	<div className="overlay">
	            		<div className="caption">
		            		<p className="small-heading">Asian Paints</p>
		            		<p className="heading">An Untold Story</p>
		            		<a href="" className="btn">View Case Study</a>
		            	</div>
	            	</div>
	            </div>
	            <div className="swiper-slide" style={sliderone}>
	            	<div className="overlay">
	            		<div className="caption">
		            		<p className="small-heading">Asian Paints</p>
		            		<p className="heading">An Untold Story</p>
		            		<a href="" className="btn">View Case Study</a>
		            	</div>
	            	</div>
	            </div>
	        </div>
	        <div className="pagination">
	        	<div className="swiper-pagination"></div>
	        	 <div className="swiper-button-prev"></div>
  				<div className="swiper-button-next"></div>
	        </div>
    	</div>
       </div>
      </div>
      
    );
  }

}