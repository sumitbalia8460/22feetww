import React from "react";


export default class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
      	<div className="container">
      		<div className="social">
              <ul>
                <a href="#"><li className="fb"></li></a>
                <a href="#"><li className="insta"></li></a>
                <a href="#"><li className="tw"></li></a>
              </ul>
            </div>
            <p className="mb5">hr@22feettribalww.com</p>
            <p className="rights">&#169; 22feet tribal worldwide pvt.ltd. all rights reserved.</p>
      	</div>
      </footer>
    );
  }
}
